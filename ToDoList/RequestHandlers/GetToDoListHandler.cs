using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marten;
using MediatR;
using ToDoList.Models;
using ToDoList.Requests;

namespace ToDoList.RequestHandlers
{
    public class GetToDoListHandler : AsyncRequestHandler<GetToDoList, List<ToDo>>
    {
        private readonly IDocumentSession _documentSession;

        public GetToDoListHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        protected override async Task<List<ToDo>> HandleCore(GetToDoList request)
        {
            var toDos = await _documentSession.Query<ToDo>().OrderBy(x => x.Order).ToListAsync();

            return toDos.ToList();
        }
    }
}
