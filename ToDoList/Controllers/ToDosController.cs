using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Requests;

namespace ToDoList.Controllers
{
    [Route("api/[controller]")]
    public class ToDosController : Controller
    {
        private readonly IMediator _mediator;

        public ToDosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetToDoList()
        {
            var response = await _mediator.Send(new GetToDoList());

            return Ok(response);
        }
    }
}
