import { ToDoListContainerComponent } from './to-do-list-container';

export const containers: any[] = [
    ToDoListContainerComponent,
];

export * from './to-do-list-container';
