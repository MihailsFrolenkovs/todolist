import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { ToDoService } from '../entities/services/to-do';
import * as toDoActions from '../entities/actions/to-do';
import { Observable } from 'rxjs';
import { ToDo } from '../entities/models/to-do';
import * as fromEntitiesStore from '../entities/store';

@Component({
  selector: 'app-to-do-list-container',
  templateUrl: './to-do-list-container.html',
  styleUrls: ['./to-do-list-container.scss']
})
export class ToDoListContainerComponent implements OnInit {

  toDoList$: Observable<ToDo[]>;

  constructor(private store: Store<any>) {
    this.toDoList$ = store.select(fromEntitiesStore.getToDoList);
  }

  ngOnInit() {
  }

  onAddToDo($event) {
    this.store.dispatch(new toDoActions.AddToDo({ toDo: { description: $event.description } }));
  }

  onChangeToDoOrder($event) {
    this.store.dispatch(new toDoActions.ChangeToDoOrder($event));
  }

}
