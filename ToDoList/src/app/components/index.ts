import { ToDoAddBarComponent } from './to-do-add-bar';
import { ToDoListComponent } from './to-do-list';
import { ToDoItemComponent } from './to-do-item';

export const components: any[] = [
    ToDoAddBarComponent,
    ToDoListComponent,
    ToDoItemComponent,
];

export * from './to-do-add-bar';
export * from './to-do-list';
export * from './to-do-item';
