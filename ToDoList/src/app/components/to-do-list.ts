import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { ToDo } from '../entities/models/to-do';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.html',
  styleUrls: ['./to-do-list.scss']
})
export class ToDoListComponent implements OnInit, OnChanges {

  @Input() toDos: ToDo[];

  @Output() addToDo = new EventEmitter();
  @Output() deleteToDo = new EventEmitter();
  @Output() changeToDoOrder = new EventEmitter();

  sortablejsOptions;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  onAddTodo($event) {
    this.addToDo.emit($event);
  }

  onItemDelete($event) {
    this.deleteToDo.emit($event);
  }

}
