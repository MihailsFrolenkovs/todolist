import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-to-do-add-bar',
  templateUrl: './to-do-add-bar.html',
  styleUrls: ['./to-do-add-bar.scss']
})
export class ToDoAddBarComponent implements OnInit {

  @Output() addToDo = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit() {
  }

}
