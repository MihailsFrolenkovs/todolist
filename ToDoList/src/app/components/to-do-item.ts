import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ToDo } from '../entities/models/to-do';

@Component({
    selector: 'app-to-do-item',
    templateUrl: './to-do-item.html',
    styleUrls: ['./to-do-item.scss']
})
export class ToDoItemComponent implements OnInit {

    @Input() toDo: ToDo;

    @Output() delete = new EventEmitter();

    constructor() { }

    ngOnInit(): void {

    }

    onDelete() {
        this.delete.emit();
    }
}
