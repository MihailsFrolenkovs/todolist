import { ToDoEffects } from './to-do';

export const effects: any[] = [
    ToDoEffects,
];

export * from './to-do';
