import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
  AddToDoFail,
  GetToDoList,
  GetToDoListSuccess,
  ToDoActionTypes,
} from '../actions/to-do';
import { ToDo } from '../models/to-do';
import { ToDoService } from '../services/to-do';

@Injectable()
export class ToDoEffects {

  constructor(private actions$: Actions, private toDoService: ToDoService) { }

  @Effect()
  getList$ = this.actions$
    .ofType(ToDoActionTypes.GetToDoList).pipe(
      map((action: GetToDoList) => action),
      switchMap(action =>
        this.toDoService.getToDoList().pipe(
          map((toDos: ToDo[]) => new GetToDoListSuccess(toDos)),
          catchError(error => of(new AddToDoFail(error)))
        )
      )
    );
}
