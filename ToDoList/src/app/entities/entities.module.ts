import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import * as fromEffects from './effects';
import * as fromServices from './services';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('entities', reducers),
    EffectsModule.forFeature([...fromEffects.effects]),
    HttpClientModule,
  ],
  declarations: [],
  providers: [...fromServices.services],
})
export class EntitiesModule { }
