import * as fromTodos from './to-do';
import * as fromRoot from '../../reducers';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface EntitiesState {
    todos: fromTodos.State;
}

export interface State extends fromRoot.State {
    'entities': EntitiesState;
}

export const reducers: ActionReducerMap<EntitiesState> = {
    todos: fromTodos.reducer,
};

export const getEntitiesState = createFeatureSelector<EntitiesState>('entities');

// Forms

export const getToDoState = createSelector(
    getEntitiesState,
    state => state.todos
);

export const {
    selectAll: getToDoList,
} = fromTodos.adapter.getSelectors(getToDoState);
