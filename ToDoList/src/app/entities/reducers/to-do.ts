import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { ToDo } from '../models/to-do';
import { ToDoActions, ToDoActionTypes } from '../actions/to-do';

export interface State extends EntityState<ToDo> {
}

export const adapter: EntityAdapter<ToDo> = createEntityAdapter<ToDo>();

export const initialState: State = adapter.getInitialState({
});

export function reducer(
  state = initialState,
  action: ToDoActions
): State {
  switch (action.type) {

    case ToDoActionTypes.GetToDoListSuccess: {
      return adapter.addAll(action.payload, state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
