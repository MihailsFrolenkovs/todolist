import { Injectable } from '@angular/core';
import { ToDo } from '../models/to-do';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {
  API_PATH = 'api/todos';

  constructor(private http: HttpClient) { }

  getToDoList(): Observable<ToDo[]> {
    return this.http
      .get<ToDo[]>(`${this.API_PATH}`).pipe(
        map(res => res),
      );
  }
}
