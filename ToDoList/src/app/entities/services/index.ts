import { ToDoService } from './to-do';

export const services: any[] = [
    ToDoService,
];

export * from './to-do';
