import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { ToDo } from '../models/to-do';

export enum ToDoActionTypes {
  GetToDoList = '[ToDo] Load ToDo List',
  GetToDoListSuccess = '[ToDo] Load ToDo List Success',
  GetToDoListFail = '[ToDo] Load ToDo List Fail',

  AddToDo = '[ToDo] Add ToDo',
  AddToDoSuccess = '[ToDo] Add ToDo Success',
  AddToDoFail = '[ToDo] Add ToDo Fail',

  ChangeToDoOrder = '[ToDo] Change ToDo Order',
  ChangeToDoOrderSuccess = '[ToDo] Change ToDo Order Success',
  ChangeToDoOrderFail = '[ToDo] Change ToDo Order Fail',
}

export class GetToDoList implements Action {
  readonly type = ToDoActionTypes.GetToDoList;
}

export class GetToDoListSuccess implements Action {
  readonly type = ToDoActionTypes.GetToDoListSuccess;

  constructor(public payload: ToDo[]) { }
}

export class GetToDoListFail implements Action {
  readonly type = ToDoActionTypes.GetToDoListFail;

  constructor(public payload: any) { }
}

export class AddToDoSuccess implements Action {
  readonly type = ToDoActionTypes.AddToDoSuccess;

  constructor(public payload: { toDo: ToDo }) { }
}

export class AddToDoFail implements Action {
  readonly type = ToDoActionTypes.AddToDoFail;

  constructor(public payload: any) { }
}

export class AddToDo implements Action {
  readonly type = ToDoActionTypes.AddToDo;

  constructor(public payload: { toDo: any }) { }
}

export class ChangeToDoOrder implements Action {
  readonly type = ToDoActionTypes.ChangeToDoOrder;

  constructor(public payload: string[]) { }
}

export class ChangeToDoOrderSuccess implements Action {
  readonly type = ToDoActionTypes.ChangeToDoOrderSuccess;
}

export class ChangeToDoOrderFail implements Action {
  readonly type = ToDoActionTypes.ChangeToDoOrderFail;
}

export type ToDoActions =
  GetToDoList
  | GetToDoListSuccess
  | GetToDoListFail
  | AddToDo
  | AddToDoSuccess
  | AddToDoFail
  | ChangeToDoOrder
  | ChangeToDoOrderSuccess
  | ChangeToDoOrderFail;
