export interface ToDo {
  id: string;
  description: string;
  order: number;
}
