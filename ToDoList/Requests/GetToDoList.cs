using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using ToDoList.Models;

namespace ToDoList.Requests
{
    public class GetToDoList: IRequest<List<ToDo>>
    {
    }
}
