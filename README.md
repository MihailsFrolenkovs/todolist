# To-do app

## Built with
- [dotnet/core](https://github.com/dotnet/core) - Backend
  - [Marten](https://github.com/JasperFx/marten/) - Postgresql as a document database
  - [MediatR](https://github.com/jbogard/MediatR) - CQRS
- [@angular-cli](https://github.com/angular/angular-cli) - Frontend
  - [@ngrx](https://github.com/ngrx/platform) - Redux powered by RxJS
  - [Material](https://github.com/angular/material2) - UI
  
## Getting started
It is recommended to use [Visual Studio 2017](https://www.visualstudio.com/vs/) and/or [Visual Studio Code](https://code.visualstudio.com/) for development.

### Before setup
Make sure you have installed:
- [yarn](https://yarnpkg.com)
- [PostgreSQL](https://www.postgresql.org/)

### Installing
Clone this repo and open solution with VS/VS Code. 

Run `yarn install` to install all dependencies and then `yarn start` to build Angular project. This command also enables watch mode - project will be rebuilt after changes are detected. 

Add `connectionstrings.json`. The following structure should be used:
```
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=localhost;Port=5432;Database=todos;User Id=postgres;Password=***;"
  }
}
```

Finally you can run solution from the VS/using command line.

## Task
Your main task is to replicate functionality of the app located at http://todo.claviz.com/.
Some features are already implemented in this project, but some of them are missing.
When everytihng is done, push your changes to some publically available repository (https://bitbucket.com or even https://github.com/) and send us it's link.

### Main points to consider
* User should be able to view/add/remove/reorder to-do items.
* To-do items should be persisted in the database.
